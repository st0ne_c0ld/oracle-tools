set lines 300
set pages 500
col total_gb format 990.99
col used_gb format 990.99
col free_gb format 990.99
SELECT * FROM
  (SELECT tablespace_name FROM dba_tablespaces)
LEFT OUTER JOIN
  (SELECT tablespace_name, SUM(bytes)/1024/1024/1024 AS total_GB
     FROM dba_data_files
     GROUP BY tablespace_name)
  USING (tablespace_name)
LEFT OUTER JOIN
  (SELECT tablespace_name, sum(bytes)/1024/1024/1024 AS used_GB
     from dba_segments
     GROUP BY tablespace_name)
  USING (tablespace_name)
LEFT OUTER JOIN
  (SELECT tablespace_name, SUM(bytes)/1024/1024/1024 AS free_GB
     FROM dba_free_space
     GROUP BY tablespace_name)
  USING (tablespace_name);