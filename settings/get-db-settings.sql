-- Collect info about the database

column passout new_value dbname noprint
SELECT TO_CHAR(SYSDATE,'MM/DD/YY') TODAY,
       TO_CHAR(SYSDATE,'HH:MI AM') TIME,
       --DATABASE||' Database' DATABASE,
       --rtrim(database) passout
       name||' Database' DATABASE,
       lower(rtrim(name)) passout
FROM   v$database;
rem
set heading on
set pagesize 58
set line 80
set newpage 0
define db = '_&dbname'



spool &dbname.log

-- INIT
SET NEWPAGE 0 VERIFY OFF
SET PAGES 10000 lines 131
COLUMN name  format a37
COLUMN value format a30
COLUMN description format a40 word_wrapped
Ttitle "INIT.ORA PARAMETER LISTING"
SELECT   NAME, VALUE, description
    FROM v$parameter
    where isdefault = 'FALSE'
ORDER BY NAME;
CLEAR COLUMNS
SET VERIFY ON termout on PAGES 22 lines 80
UNDEF output

-- Character set
SET PAGES 10000 lines 131
COLUMN name  format a37
COLUMN value format a30
Ttitle "Character set settings"
SELECT 
	* 
FROM 
	NLS_DATABASE_PARAMETERS
;
TTITLE OFF
clear columns;


-- REDO
SET PAGES 10000 lines 131
COLUMN member format a32
Ttitle "Redo Log Groups"
select a.Group#, a.Type, a.member, b.bytes/1024/1024 as MB
FROM
        v$logfile a,
        v$Log b
where
        a.Group# = b.group#
;
TTITLE OFF
clear columns;

-- DATAFILES n TABLESPACES
CLEAR computes
SET PAGES 10000 lines 131
COLUMN FILE_NAME                FORMAT A40
COLUMN TABLESPACE_NAME          FORMAT A16
COLUMN ENCRYPTED                FORMAT A3
COLUMN MEG                      FORMAT 99,999.90
Ttitle 'Database Datafiles'
BREAK ON TABLESPACE_NAME SKIP 1 ON ENCRYPTED SKIP 1 ON REPORT

COMPUTE SUM OF MEG ON a.TABLESPACE_NAME
COMPUTE sum of meg on REPORT
SELECT   b.tablespace_name, b.ENCRYPTED, a.file_name, a.bytes / 1048576 meg
    FROM dba_data_files a, dba_tablespaces b
where
        a.tablespace_name = b.tablespace_name
ORDER BY tablespace_name
/

CLEAR columns
CLEAR computes



spool off
quit;
