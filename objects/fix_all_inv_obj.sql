set lines 300
set pages 500

spool inv_objs.sql
--compile body
select
    'alter package '||owner||'.'||object_name||' compile body;',
from
    all_objects
where
    status<>'VALID' and
    object_type='PACKAGE BODY'
;
--compile pkgs
select
    'alter package '||owner||'.'||object_name||' compile;'
from
    all_objects
where
    status<>'VALID' and
    object_type='PACKAGE BODY'
;

spool off   
