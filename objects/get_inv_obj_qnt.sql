set lines 300
set pages 500
select 
    owner ||'.'||
    object_type,
    count(*)
from
    all_objects
where
    status <> 'VALID'
group by
    owner,
    object_type
;
