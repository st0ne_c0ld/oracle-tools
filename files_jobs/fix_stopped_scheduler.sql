-- Undocumented method to fix stopped scheduler
-- If this was not help - ask for restart
exec dbms_scheduler.set_scheduler_attribute('SCHEDULER_DISABLED', 'TRUE');
alter system set job_queue_processes=0;
exec dbms_ijob.set_enabled(FALSE);

alter system flush shared_pool;
alter system flush shared_pool;

exec dbms_ijob.set_enabled(TRUE);
alter system set job_queue_processes=99;
exec dbms_scheduler.set_scheduler_attribute('SCHEDULER_DISABLED', 'FALSE');
